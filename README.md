# Projekt Automatisierungstechnik

## "Drehbuch"

  - [ ] vor 29.3.  
    - [x] ROS kennenlernen  
    - [ ] ROS Skripte vorbereiten  
  - [ ] am 29.3.  
    - [ ] Betrieb mit WLAN  
    - [ ] ROS Knoten realisieren  
  - [ ] vor/am 5.4.  
    - [ ] TOF Sensor auslesen, auswerten, Winkel ausgeben  
  - [ ] vor/am 12.4.  
    - [ ] Simulink zu ROS verstehen  
    - [ ] Kalmanfilter in Simulink umsetzen

## ROS
Die Python Skripte für die ROS-Knoten werden im catkin_ws (workspace) unter `catkin_ws/src/scripts/` abgelegt,
so können sie einfach mit `rosrun pat_ros my_node` gestartet werden.


Um nicht jeden Knoten einzeln starten zu müssen wird `roslaunch` verwendet.
Es nutzt eine Art config-Datei unter `catkin_ws/src/launch/` in der beschrieben ist welcher Knoten wo und wie gestartet werden soll.
`roslaunch` führt für uns `roscore` und `rosrun` aus.

Der Aufbau des ROS-Netzwerks kann mit `rqt_graph` am PC angezeigt werden.

Alle Topics, also Nachrichten-Arten `rostopic list` kann de